/*
*@method getLetterMatchCount.
*@param {string} guessedWord - Guessed word
*@param {string} secretWord - Secret Word.
*@returns {number} - Number of letters matched between guessed word and Secret word.
*/


export function getLetterMatchCount(guessedWord, secretWord) {
    const secretLetterSet = new Set(secretWord.split(''));
    const guessedtLetterSet = new Set(guessedWord.split(''));
    return [...secretLetterSet].filter(letter => guessedtLetterSet.has(letter)).length;
};