import React from 'react';
import { shallow } from 'enzyme';

import { findByTestAttr, checkProps } from '../test/testUtils';
import App from './App';

const setup = (props={}) => {
  const setupProps = { ...props }
  return shallow(<App {...setupProps} />)
}

test('renders without error', () => {
  const wrapper = setup();
  const component = findByTestAttr(wrapper, 'component');
  expect(component.length).toBe(1); 
});